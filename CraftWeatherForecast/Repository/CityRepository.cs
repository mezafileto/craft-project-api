﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CraftWeatherForecast.Models;
using CraftWeatherForecast.DbContexts;

namespace CraftWeatherForecast.Repository
{
    public class CityRepository : ICityRepository
    {
        private static readonly string[] Forecasts = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private IApplicationDbContext _dbcontext;
        public CityRepository(IApplicationDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        public async Task<List<City>> GetAll()
        {
            var citys = await _dbcontext.City.ToListAsync<City>();
            return citys;
        }

        public async Task<City> GetById(int id)
        {
            var city = await _dbcontext.City.Where(cty => cty.CityId == id).FirstOrDefaultAsync();
            return city;
        }

        public async Task<List<CityWeatherDto>> GetWeatherByCityId(int id)
        {
            DateTime dt_today = DateTime.Now;
            int weekStart = (int)dt_today.DayOfWeek;
            DateTime weekEnd = dt_today.AddDays(7 - weekStart);

            var cityWeathersOriginalTransactions = await _dbcontext.CityWeather.Where(a => a.FK_CityId == id && (a.WeatherDate >= dt_today.Date && a.WeatherDate <= weekEnd.Date) && a.ActiveStatus).ToListAsync();
            var rng = new Random();
            var cityWeathersRecords = (from ch in cityWeathersOriginalTransactions
                                       select new CityWeatherDto
                                   {
                                           CityWeatherId = ch.CityWeatherId,
                                           TemperatureC = ch.TemperatureC,
                                           TemperatureF = ch.TemperatureF,
                                           Forecast = Forecasts[rng.Next(Forecasts.Length)],
                                           WeatherDate = ch.WeatherDate
                                       }).ToList();

            return cityWeathersRecords;
        }
    }
}
