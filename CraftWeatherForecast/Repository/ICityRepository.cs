﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CraftWeatherForecast.Models;

namespace CraftWeatherForecast.Repository
{
    public interface ICityRepository
    {
        Task<List<City>> GetAll();
        Task<City> GetById(int id);
        Task<List<CityWeatherDto>> GetWeatherByCityId(int id);
    }
}
