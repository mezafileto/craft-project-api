﻿using CraftWeatherForecast.Models;
using Microsoft.EntityFrameworkCore;

namespace CraftWeatherForecast.DbContexts
{
    public interface IApplicationDbContext
    {
        DbSet<City> City { get; set; }
        DbSet<CityWeather> CityWeather { get; set; }

    }
}
