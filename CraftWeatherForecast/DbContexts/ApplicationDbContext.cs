﻿using CraftWeatherForecast.Models;
using Microsoft.EntityFrameworkCore;

namespace CraftWeatherForecast.DbContexts
{
    public class ApplicationDbContext: DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base (options) 
        {
        }
        public DbSet<City> City { get; set; }
        public DbSet<CityWeather> CityWeather { get; set; }

    }
}
