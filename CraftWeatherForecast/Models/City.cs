﻿using System;

namespace CraftWeatherForecast.Models
{
    public class City
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string CityDescription { get; set; }
        public string CityCoverImg { get; set; }
        public bool ActiveStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string LastModifiedByUser { get; set; }
    }
}