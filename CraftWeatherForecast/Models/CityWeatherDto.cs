﻿using System;

namespace CraftWeatherForecast.Models
{
    public class CityWeatherDto
    {
        public int CityWeatherId { get; set; }
        public int TemperatureC { get; set; }
        public int TemperatureF { get; set; }
        public string Forecast { get; set; }
        public DateTime WeatherDate { get; set; }
    }
}