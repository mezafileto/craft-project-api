﻿using System;

namespace CraftWeatherForecast.Models
{
    public class CityWeather
    {
        public int CityWeatherId { get; set; }
        public int FK_CityId { get; set; }
        public int TemperatureC { get; set; }
        public int TemperatureF { get; set; }
        public DateTime WeatherDate { get; set; }
        public bool ActiveStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string LastModifiedByUser { get; set; }
    }
}