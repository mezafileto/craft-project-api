﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using CraftWeatherForecast.Models;
using CraftWeatherForecast.Repository;


namespace CraftWeatherForecast.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {

        private ICityRepository _dbcontext;
        public CityController(ICityRepository dbcontext)
        {
            _dbcontext = dbcontext;
        }

        /// <summary>
        /// Get All Cities.
        /// </summary>
        /// <returns>List<City></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<City>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Route("GetAll")]
        public async Task<ActionResult> GetAll()
        {
            var cities = await _dbcontext.GetAll();
            return Ok(cities);
        }

        /// <summary>
        /// Get City By Id.
        /// </summary>
        /// <param name="id">The iD of the current city</param>
        /// <returns>Gets a list of cities</returns>
        [HttpGet]
        [ProducesResponseType(typeof(City), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Route("GetById/{id}")]
        public async Task<ActionResult> GetById(int id)
        {
            var citie = await _dbcontext.GetById(id);
            return Ok(citie);
        }

        /// <summary>
        /// Get All Weathers By CityId.
        /// </summary>
        /// <param name="id">The Id of the current city you want to get the weather</param>
        /// <returns>List<CityWeatherDto></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<CityWeatherDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Route("GetWeatherByCityId/{id}")]
        public async Task<ActionResult> GetWeatherByCityId(int id)
        {
            var citiesWeather = await _dbcontext.GetWeatherByCityId(id);
            return Ok(citiesWeather);
        }

    }
}
